package com.homework.conveyor;

/**
 * Created by Valeriy on 31.10.2016.
 */
public class Conveyor {
    public static void main(String[] args) {
        Worker vasya = new Worker("Vasya", "Thread 1");
        Worker petya = new Worker("Petya","Thread 2");

        vasya.start();
        try {
            vasya.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        petya.start();
    }
}

class Worker extends Thread{
    private String workerName,threadName;

    public Worker(String workerName, String threadName) {
        this.workerName=workerName;
        this.threadName=threadName;
    }

    @Override
    public void run() {
        super.run();
        System.out.println("Worker "+workerName+" started to work! "+ threadName);
        for (int i=0;i<1000;i++){
            System.out.println("Detail number "+i+" is done by "+workerName);
        }
    }
}
