package com.homework.carTotalizator;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Valeriy on 31.10.2016.
 */
public class CarTotalizator {
    private static int luckyNumber;
    public static void main(String[] args) {
        List<Car> threads = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Write number 1%10");
        luckyNumber=scanner.nextInt();
        if(luckyNumber<1||luckyNumber>10){
            System.out.println("Bad number");
            return;
        }
        for(int i=0;i<10;i++){
            Car car = new Car(i,"Thread 1");
            if(i==luckyNumber)
                car.setPriority(10);
            else
                car.setPriority(1);
                threads.add(car);
            threads.get(i).start();
        }

    }
}

class Car extends Thread{
    private int carNumber;
    private String threadName;

    public Car(int carNumber, String threadName) {
        this.carNumber = carNumber;
        this.threadName = "Car with number: "+carNumber +" Thread name: "+threadName;
    }

    @Override
    public void run() {
        super.run();
        for (int i=0;i<10;i++){
            System.out.println("["+i+"] "+threadName);
        }
        System.out.println("Car with number "+carNumber+" is finished!");
    }
}
