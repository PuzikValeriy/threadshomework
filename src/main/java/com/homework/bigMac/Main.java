package com.homework.bigMac;

/**
 * Created by Valeriy on 04.11.2016.
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Join:");
        Thread hamburger = new Thread(()->{
            System.out.println("Hamburger cooking started");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Hamburger is ready!");
        });

        Thread fries = new Thread(()->{
            System.out.println("Fries cooking started");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                hamburger.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Fries is ready!");
        });

        Thread cola = new Thread(()->{
            System.out.println("Cola cooking started");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                fries.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Cola is ready!");
        });

        Thread bigMakMenu = new Thread(()-> {
            try {
                cola.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("BigMacMenu is ready!");
        });
        hamburger.start();
        fries.start();
        cola.start();
        bigMakMenu.start();
    }
}
