package com.homework.bigMac;

/**
 * Created by Valeriy on 04.11.2016.
 */
public class BigMacMenu{
    private boolean isHamburgerReady;
    private boolean isFriesReady;
    private boolean isColaReady;
    private boolean isBigMacMenuReady;

    public boolean isHamburgerReady() {
        return isHamburgerReady;
    }

    public void setHamburgerReady(boolean isHamburgerReady) {
        this.isHamburgerReady = isHamburgerReady;
    }

    public boolean isFriesReady() {
        return isFriesReady;
    }

    public void setFriesReady(boolean isFriesReady) {
        this.isFriesReady = isFriesReady;
    }

    public boolean isColaReady() {
        return isColaReady;
    }

    public void setColaReady(boolean isColaReady) {
        this.isColaReady = isColaReady;
    }

    public void setBigMacMenuReady(boolean isBigMacMenuReady) {
        this.isBigMacMenuReady = isBigMacMenuReady;
    }
}

class McDonaldsKitchen extends Thread{
    final BigMacMenu bigMacMenu = new BigMacMenu();

    public void createBigMacMenu() {
        System.out.println("Synchronized with wait+notifyAll:");
        Thread hamburger = new Thread(() -> {
            synchronized (bigMacMenu) {
                System.out.println("Hamburger cooking started");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Hamburger is ready!");
                bigMacMenu.setHamburgerReady(true);
                bigMacMenu.notifyAll();
            }
        });

        Thread fries = new Thread(() -> {
            synchronized (bigMacMenu) {
                System.out.println("Fries cooking started");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    while (!bigMacMenu.isHamburgerReady()) {
                        System.out.println("Waiting for hamburger");
                        bigMacMenu.wait();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Fries is ready!");
                bigMacMenu.setFriesReady(true);
                bigMacMenu.notifyAll();
            }
        });

        Thread cola = new Thread(() -> {
            synchronized (bigMacMenu) {
                System.out.println("Cola cooking started");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    while (!bigMacMenu.isFriesReady()) {
                        System.out.println("Waiting for fries");
                        bigMacMenu.wait();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Cola is ready!");
                bigMacMenu.setColaReady(true);
                bigMacMenu.notifyAll();
            }
        });

        Thread bigMakMenus = new Thread(() -> {
            synchronized (bigMacMenu) {
                try {
                    while (!bigMacMenu.isColaReady())
                        bigMacMenu.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                bigMacMenu.setBigMacMenuReady(true);
                System.out.println("BigMacMenu is ready!");
            }
        });
        hamburger.start();
        fries.start();
        cola.start();
        bigMakMenus.start();
    }

    public static void main(String[] args) throws InterruptedException {
        McDonaldsKitchen mcDonaldsKitchen = new McDonaldsKitchen();
        mcDonaldsKitchen.createBigMacMenu();
    }
}

