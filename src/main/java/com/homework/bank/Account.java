package com.homework.bank;

/**
 * Created by Valeriy on 04.11.2016.
 */
public class Account{
    volatile Long balance = 0L;

    public Account(Long balance) {
        this.balance = balance;
    }

    synchronized void add(Long money){
        balance+=money;
        System.out.println("Add "+money+". New balance: "+balance);}

    synchronized void withdraw(Long money){
        balance-=money;
        System.out.println("Withdraw "+money+". New balance: "+balance);}
}
