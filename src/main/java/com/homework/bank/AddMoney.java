package com.homework.bank;

/**
 * Created by Valeriy on 04.11.2016.
 */
public class AddMoney extends Thread {
    public AddMoney() {
        this.start();
    }

    @Override
    public void run() {
        while (Bank.isBankomatWorking) {
            Bank.account.add(1000L);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
