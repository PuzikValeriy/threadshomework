package com.homework.bank;
/**
 * Created by Valeriy on 04.11.2016.
 */
public class Bank {
    static Account account = new Account(1000L);
    public static volatile boolean isBankomatWorking=true;

    public static void main(String[] args) throws InterruptedException {
        new AddMoney();
        new WithdrawMoney();
        new WithdrawMoney();
        new WithdrawMoney();
        new WithdrawMoney();

        Thread.sleep(3000);
        isBankomatWorking=false;
    }
}
