package com.homework.bank;

/**
 * Created by Valeriy on 04.11.2016.
 */
public class WithdrawMoney extends Thread{
    public WithdrawMoney() {
        this.start();
    }

    @Override
    public void run() {
        while (Bank.isBankomatWorking){
            Bank.account.withdraw(500L);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
